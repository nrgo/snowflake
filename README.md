# snowflake

#### 介绍
snowflake(forked from twitter) for nrgo



#### 使用说明

```go
// An ID is a custom type used for a snowflake ID.  This is used so we can
// attach methods onto the ID.
type ID int64

// NewNode returns a new snowflake node that can be used to generate snowflake
// IDs
func NewNode(node int64) (*Node, error)

// Generate creates and returns a unique snowflake ID
// To help guarantee uniqueness
// - Make sure your system is keeping accurate system time
// - Make sure you never have multiple nodes running with the same node ID
func (n *Node) Generate() ID

// Int64 returns an int64 of the snowflake ID
func (f ID) Int64() int64

// String returns a string of the snowflake ID
func (f ID) String() string 

// Base2 returns a string base2 of the snowflake ID
func (f ID) Base2() string

// Base32 uses the z-base-32 character set but encodes and decodes similar
// to base58, allowing it to create an even smaller result string.
// NOTE: There are many different base32 implementations so becareful when
// doing any interoperation.
func (f ID) Base32() string

// Base36 returns a base36 string of the snowflake ID
func (f ID) Base36() string

// Base58 returns a base58 string of the snowflake ID
func (f ID) Base58() string

// Base64 returns a base64 string of the snowflake ID
func (f ID) Base64() string

// Bytes returns a byte slice of the snowflake ID
func (f ID) Bytes() []byte

// IntBytes returns an array of bytes of the snowflake ID, encoded as a
// big endian integer.
func (f ID) IntBytes() [8]byte

// Time returns an int64 unix timestamp in milliseconds of the snowflake ID time
// Deprecated: the below function will be removed in a future release.
func (f ID) Time() int64

// Node returns an int64 of the snowflake ID node number
// Deprecated: the below function will be removed in a future release.
func (f ID) Node() int64

// Step returns an int64 of the snowflake step (or sequence) number
// Deprecated: the below function will be removed in a future release.
func (f ID) Step() int64

// MarshalJSON returns a json byte array string of the snowflake ID.
func (f ID) MarshalJSON() ([]byte, error)

// UnmarshalJSON converts a json byte array of a snowflake ID into an ID type.
func (f *ID) UnmarshalJSON(b []byte) error

// ParseInt64 converts an int64 into a snowflake ID
func ParseInt64(id int64) ID

// ParseString converts a string into a snowflake ID
func ParseString(id string) (ID, error)

// ParseBase2 converts a Base2 string into a snowflake ID
func ParseBase2(id string) (ID, error)

// ParseBase32 parses a base32 []byte into a snowflake ID
// NOTE: There are many different base32 implementations so becareful when
// doing any interoperation.
func ParseBase32(b []byte) (ID, error)

// ParseBase36 converts a Base36 string into a snowflake ID
func ParseBase36(id string) (ID, error)

// ParseBase58 parses a base58 []byte into a snowflake ID
func ParseBase58(b []byte) (ID, error)

// ParseBase64 converts a base64 string into a snowflake ID
func ParseBase64(id string) (ID, error)

// ParseBytes converts a byte slice into a snowflake ID
func ParseBytes(id []byte) (ID, error)

// ParseIntBytes converts an array of bytes encoded as big endian integer as
// a snowflake ID
func ParseIntBytes(id [8]byte) ID
```
